using UnityEngine;




namespace PossumScream.ExtremeExtensions
{
	public static class BoolExtensions
	{
		public static int AsInteger(this bool logicalValue)
		{
			return (logicalValue ? 1 : 0);
		}


		public static int AsSign(this bool logicalValue)
		{
			return (logicalValue ? 1 : -1);
		}
	}
}




/*                                                                                */
/*        David Tabernero M. @ PossumScream          Copyright © 2021-2022        */
/*        https://gitlab.com/possumscream             All rights reserved.        */
/*                                                                                */