using UnityEngine;




namespace PossumScream.ExtremeExtensions
{
	public static class FloatExtensions
	{
		#region Controls: Verification


			public static bool IsBetween(this float testValue, int minInclusive, int maxExclusive)
			{
				return ((testValue >= minInclusive) && (testValue < maxExclusive));
			}


			public static bool IsBetweenInclusive(this float testValue, int minInclusive, int maxInclusive)
			{
				return ((testValue >= minInclusive) && (testValue <= maxInclusive));
			}


			public static bool IsBetweenExclusive(this float testValue, int minExclusive, int maxExclusive)
			{
				return ((testValue > minExclusive) && (testValue < maxExclusive));
			}


		#endregion




		#region Controls: Operation


			public static float SumToAbsolute(this float currentValue, float addend)
			{
				return (Mathf.Sign(currentValue) * (Mathf.Abs(currentValue) + addend));
			}


			public static float SubstractToAbsolute(this float currentValue, float substrahend)
			{
				return (Mathf.Sign(currentValue) * (Mathf.Abs(currentValue) - substrahend));
			}


			public static float MultiplyToAbsolute(this float currentValue, float factor)
			{
				return (Mathf.Sign(currentValue) * (Mathf.Abs(currentValue) * factor));
			}


			public static float DivideToAbsolute(this float currentValue, float divisor)
			{
				return (Mathf.Sign(currentValue) * (Mathf.Abs(currentValue) / divisor));
			}




			public static float AddAsAverage(this ref float currentAverage, float newValue, ref int currentSamples, int maxSamples)
			{
				float totalValue = currentAverage * currentSamples;


				{
					if (currentSamples >= maxSamples) {
						currentSamples--;
						totalValue -= currentAverage;
					}

					{
						totalValue += newValue;
						currentSamples++;
					}

					if (currentSamples > maxSamples) {
						currentSamples--;
						totalValue -= currentAverage;
					}
				}

				return (currentAverage = (totalValue / currentSamples));
			}


		#endregion
	}
}




/*                                                                                */
/*        David Tabernero M. @ PossumScream          Copyright © 2021-2022        */
/*        https://gitlab.com/possumscream             All rights reserved.        */
/*                                                                                */