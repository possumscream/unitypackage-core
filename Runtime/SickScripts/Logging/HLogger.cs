using System;
using UnityEngine;
using UnityObject = UnityEngine.Object;




namespace PossumScream.SickScripts.Logging
{
	public static class HLogger
	{
		public const string emphasisColor = "#64c8fa";
		public const string errorColor = "#fa6464";
		public const string exceptionColor = "#c864fa";
		public const string infoColor = "#fafafa";
		public const string warningColor = "#fac864";




		#region Controls


			public static void log(string message)
			{
				#if !LOGGING_DISABLED
					HLogger.logInfo(message);
				#endif
			}


			public static void log(string message, Type context)
			{
				#if !LOGGING_DISABLED
					HLogger.logInfo(message, context);
				#endif
			}


			public static void log(string message, UnityObject context)
			{
				#if !LOGGING_DISABLED
					HLogger.logInfo(message, context);
				#endif
			}




			public static void logInfo(string message)
			{
				#if !LOGGING_DISABLED
					Debug.Log(message);
				#endif
			}


			public static void logInfo(string message, Type context)
			{
				#if !LOGGING_DISABLED
					Debug.Log(composeFormattedMessage(message, context, infoColor));
				#endif
			}


			public static void logInfo(string message, UnityObject context)
			{
				#if !LOGGING_DISABLED
					Debug.Log(composeFormattedMessage(message, context, infoColor), context);
				#endif
			}




			public static void logEmphasis(string message)
			{
				#if !LOGGING_DISABLED
					Debug.Log(message);
				#endif
			}


			public static void logEmphasis(string message, Type context)
			{
				#if !LOGGING_DISABLED
					Debug.Log(composeFormattedMessage(message, context, emphasisColor));
				#endif
			}


			public static void logEmphasis(string message, UnityObject context)
			{
				#if !LOGGING_DISABLED
					Debug.Log(composeFormattedMessage(message, context, emphasisColor), context);
				#endif
			}




			public static void logWarning(string message)
			{
				#if !LOGGING_DISABLED
					Debug.LogWarning(message);
				#endif
			}


			public static void logWarning(string message, Type context)
			{
				#if !LOGGING_DISABLED
					Debug.LogWarning(composeFormattedMessage(message, context, warningColor));
				#endif
			}


			public static void logWarning(string message, UnityObject context)
			{
				#if !LOGGING_DISABLED
					Debug.LogWarning(composeFormattedMessage(message, context, warningColor), context);
				#endif
			}




			public static void logError(string message)
			{
				#if !LOGGING_DISABLED
					Debug.LogError(message);
				#endif
			}


			public static void logError(string message, Type context)
			{
				#if !LOGGING_DISABLED
					Debug.LogError(composeFormattedMessage(message, context, errorColor));
				#endif
			}


			public static void logError(string message, UnityObject context)
			{
				#if !LOGGING_DISABLED
					Debug.LogError(composeFormattedMessage(message, context, errorColor), context);
				#endif
			}




			public static void logException(Exception exception)
			{
				#if !LOGGING_DISABLED
					Debug.LogException(exception);
				#endif
			}


			public static void logException(Exception exception, Type context)
			{
				#if !LOGGING_DISABLED
					Debug.LogError(composeFormattedMessage(exception.Message, context, exceptionColor));
					Debug.LogException(exception);
				#endif
			}


			public static void logException(Exception exception, UnityObject context)
			{
				#if !LOGGING_DISABLED
					Debug.LogError(composeFormattedMessage(exception.Message, context, exceptionColor), context);
					Debug.LogException(exception);
				#endif
			}


		#endregion




		#region Utilities


			public static string composeFormattedMessage(string message, Type context, string color)
			{
				return $"<color={color}><b>{context.Name} → </b></color>{message}";
			}


			public static string composeFormattedMessage(string message, UnityObject context, string color)
			{
				return $"<color={color}><b>{context.name} → </b></color>{message}";
			}


		#endregion
	}
}




/*                                                                                            */
/*            ____                                 _____                                      */
/*           / __ \____  ____________  ______ ___ / ___/_____________  ____ _____ ___         */
/*          / /_/ / __ \/ ___/ ___/ / / / __ `__ \\__ \/ ___/ ___/ _ \/ __ `/ __ `__ \        */
/*         / ____/ /_/ (__  |__  ) /_/ / / / / / /__/ / /__/ /  /  __/ /_/ / / / / / /        */
/*        /_/    \____/____/____/\__,_/_/ /_/ /_/____/\___/_/   \___/\__,_/_/ /_/ /_/         */
/*                                                                                            */
/*        Licensed under the Apache License, Version 2.0. See LICENSE.md for more info        */
/*        David Tabernero M. @ PossumScream                      Copyright © 2021-2023        */
/*        https://gitlab.com/possumscream                          All rights reserved        */
/*                                                                                            */
/*                                                                                            */