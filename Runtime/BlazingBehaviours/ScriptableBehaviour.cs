using PossumScream.SickScripts.Logging;
using System;
using UnityEngine;
using UnityObject = UnityEngine.Object;




namespace PossumScream.BlazingBehaviours
{
	public abstract class ScriptableBehaviour : MonoBehaviour
	{
		/* 0 */ [SerializeField] private string _label = null;
		/* 1 */ [SerializeField] private bool _logging = true;




		#region Controls


			protected bool log(string message = "")
			{
				return log(message, this);
			}


			protected bool log(string message, UnityObject context)
			{
				return logInfo(message, context);
			}




			protected bool logInfo(string message = "")
			{
				return logInfo(message, this);
			}


			protected bool logInfo(string message, UnityObject context)
			{
				if (this._logging) {
					{
						HLogger.logInfo(message, context);
					}
					return true;
				}


				return false;
			}




			protected bool logEmphasis(string message = "")
			{
				return logEmphasis(message, this);
			}


			protected bool logEmphasis(string message, UnityObject context)
			{
				if (this._logging) {
					{
						HLogger.logEmphasis(message, context);
					}
					return true;
				}


				return false;
			}




			protected bool logWarning(string message = "")
			{
				return logWarning(message, this);
			}


			protected bool logWarning(string message, UnityObject context)
			{
				if (this._logging) {
					{
						HLogger.logWarning(message, context);
					}
					return true;
				}


				return false;
			}




			protected bool logError(string message = "")
			{
				return logError(message, this);
			}


			protected bool logError(string message, UnityObject context)
			{
				if (this._logging) {
					{
						HLogger.logError(message, context);
					}
					return true;
				}


				return false;
			}




			protected bool logException(Exception exception)
			{
				return logException(exception, this);
			}


			protected bool logException(Exception exception, UnityObject context)
			{
				if (this._logging) {
					{
						HLogger.logException(exception, context);
					}
					return true;
				}


				return false;
			}


		#endregion




		#region Overrides


			public override string ToString()
			{
				return string.Concat(this.GetType().Name, ((this._label is not null) ? $" ({this._label})" : ""));
			}


		#endregion
	}
}




/*                                                                                            */
/*            ____                                 _____                                      */
/*           / __ \____  ____________  ______ ___ / ___/_____________  ____ _____ ___         */
/*          / /_/ / __ \/ ___/ ___/ / / / __ `__ \\__ \/ ___/ ___/ _ \/ __ `/ __ `__ \        */
/*         / ____/ /_/ (__  |__  ) /_/ / / / / / /__/ / /__/ /  /  __/ /_/ / / / / / /        */
/*        /_/    \____/____/____/\__,_/_/ /_/ /_/____/\___/_/   \___/\__,_/_/ /_/ /_/         */
/*                                                                                            */
/*        Licensed under the Apache License, Version 2.0. See LICENSE.md for more info        */
/*        David Tabernero M. @ PossumScream                      Copyright © 2021-2023        */
/*        https://gitlab.com/possumscream                          All rights reserved        */
/*                                                                                            */
/*                                                                                            */