#if UNITY_EDITOR


using UnityEditor;
using UnityEngine;




namespace PossumScream.TerrificTools
{
	public class GuidFinder : EditorWindow
	{
		private Object _retrievedObject = null;
		private bool _assetFound = false;
		private string _inputGuid = "";
		private string _retrievedPath = "";




		#region Events


			private void OnGUI()
			{
				EditorGUILayout.LabelField("Prompt", EditorStyles.boldLabel);

				this._inputGuid = EditorGUILayout.TextField("GUID", this._inputGuid);

				if (GUILayout.Button("Search in Asset Database")) {
					this._assetFound = tryFindAsset(this._inputGuid, out this._retrievedPath, out this._retrievedObject);
				}

				if (GUILayout.Button("Clean field")) {
					this._assetFound = false;
					this._inputGuid = "";
					this._retrievedObject = null;
					this._retrievedPath = "";
				}


				EditorGUILayout.LabelField("Results", EditorStyles.boldLabel);

				if (this._assetFound) {
					EditorGUILayout.LabelField("Asset found!");
					EditorGUILayout.TextField("Path", this._retrievedPath);

					if (GUILayout.Button("Highlight asset in Project")) {
						EditorGUIUtility.PingObject(this._retrievedObject);
					}

					if (GUILayout.Button("Select asset in Project")) {
						Selection.objects = new[] { this._retrievedObject };
					}
				}
				else {
					EditorGUILayout.LabelField("Asset not found.");
				}
			}


		#endregion




		#region Controls


			[MenuItem("Window/PossumScream/GUID Finder")]
			public static void createWindow()
			{
				GetWindow<GuidFinder>("GUID Finder");
			}


		#endregion




		#region Actions


			private static bool tryFindAsset(string guid, out string path, out Object unityObject)
			{
				path = AssetDatabase.GUIDToAssetPath(guid);
				unityObject = AssetDatabase.LoadAssetAtPath<Object>(path);


				return (path.Length > 0);
			}


		#endregion
	}
}


#endif




/*                                                                                */
/*                  Original script by tomekkie2 @ Unity Forums.                  */
/*                                                                                */
/*        David Tabernero M. @ PossumScream          Copyright © 2021-2022        */
/*        https://gitlab.com/possumscream             All rights reserved.        */
/*                                                                                */