#if UNITY_EDITOR


using UnityEditor;
using UnityEngine.EventSystems;
using UnityEngine;




namespace PossumScream.TerrificTools
{
	public class CurrentSelectionIndicator : EditorWindow
	{
		#region Events


			private void OnInspectorUpdate()
			{
				Repaint();
			}


			private void OnGUI()
			{
				if (Application.isPlaying) {
					if (EventSystem.current is not null && EventSystem.current.currentSelectedGameObject is not null) {
						EditorGUILayout.ObjectField("Current selection", EventSystem.current.currentSelectedGameObject, typeof(GameObject), true);
					}
					else {
						EditorGUILayout.LabelField("No GameObject selected.");
					}
				}
				else {
					EditorGUILayout.LabelField("The application must be playing to display the selected GameObject.");
				}
			}


		#endregion




		#region Controls


			[MenuItem("Window/PossumScream/Current Selection Indicator")]
			public static void createWindow()
			{
				GetWindow<CurrentSelectionIndicator>("Current Selection Indicator");
			}


		#endregion
	}
}


#endif




/*                                                                                */
/*        David Tabernero M. @ PossumScream          Copyright © 2021-2022        */
/*        https://gitlab.com/possumscream             All rights reserved.        */
/*                                                                                */