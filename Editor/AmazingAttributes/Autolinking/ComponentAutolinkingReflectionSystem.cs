#if UNITY_EDITOR


using PossumScream.SickScripts.Logging;
using System.Reflection;
using System;
using UnityEditor;
using UnityEngine;




namespace PossumScream.AmazingAttributes.Autolinking
{
	[InitializeOnLoad]
	public class ComponentAutolinkingReflectionSystem
	{
		#region Listeners


			static ComponentAutolinkingReflectionSystem()
			{
				ObjectFactory.componentWasAdded += fillComponentFields;
			}


		#endregion




		#region Controls


			[MenuItem("Tools/PossumScream/Autolinking/Autolink Components to all in scene")]
			public static void autolinkAllSceneComponents()
			{
				HLogger.logInfo("Autolinking all Components...");
				{
					fillGameobjectsComponentFields(UnityEngine.Object.FindObjectsOfType<Transform>(true));
				}
				HLogger.logInfo("Finished autolinking Components!");
			}


			[MenuItem("Tools/PossumScream/Autolinking/Autolink Components to selected in scene")]
			public static void autolinkSelectedSceneComponents()
			{
				HLogger.logInfo("Autolinking selected Components...");
				{
					fillGameobjectsComponentFields(Selection.transforms);
				}
				HLogger.logInfo("Finished autolinking Components!");
			}


		#endregion




		#region Actions


			private static void fillGameobjectsComponentFields(Transform[] transforms)
			{
				foreach (Transform currentTransform in transforms) {
					foreach (Component currentComponent in currentTransform.GetComponents<Component>()) {
						fillComponentFields(currentComponent);
					}
				}
			}


			private static void fillComponentFields(Component component)
			{
				if (component is MonoBehaviour behaviourComponent) {
					FieldInfo[] scriptFieldArchetypes = behaviourComponent.GetType().GetFields(BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic);


					foreach (FieldInfo fieldArchetype in scriptFieldArchetypes) {
						if (Attribute.GetCustomAttribute(fieldArchetype, typeof(AutolinkComponentAttribute)) is AutolinkComponentAttribute) {
							if (fieldArchetype.GetValue(behaviourComponent) is null || fieldArchetype.GetValue(behaviourComponent).Equals(null)) {
								if (behaviourComponent.TryGetComponent(fieldArchetype.FieldType, out Component valueComponent)) {
									Undo.RegisterCompleteObjectUndo(component, $"Field {fieldArchetype.Name} linked in {behaviourComponent.GetType().Name}");
									{
										fieldArchetype.SetValue(behaviourComponent, valueComponent);
									}
									PrefabUtility.RecordPrefabInstancePropertyModifications(component);
								}
							}
						}
					}
				}
			}


		#endregion
	}
}


#endif




/*                                                                                */
/*        David Tabernero M. @ PossumScream          Copyright © 2021-2022        */
/*        https://gitlab.com/possumscream             All rights reserved.        */
/*                                                                                */