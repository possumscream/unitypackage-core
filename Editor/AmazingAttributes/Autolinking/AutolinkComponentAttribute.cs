using System;




namespace PossumScream.AmazingAttributes.Autolinking
{
	[AttributeUsage(AttributeTargets.Field)]
	public class AutolinkComponentAttribute : Attribute {}
}




/*                                                                                */
/*        David Tabernero M. @ PossumScream          Copyright © 2021-2022        */
/*        https://gitlab.com/possumscream             All rights reserved.        */
/*                                                                                */