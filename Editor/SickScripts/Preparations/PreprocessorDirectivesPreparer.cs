#if UNITY_EDITOR


using PossumScream.ExtremeExtensions;
using PossumScream.SickScripts.Logging;
using System.Collections.Generic;
using System.Linq;
using System;
using UnityEditor;
using UnityEngine.Rendering;




namespace PossumScream.SickScripts.Preparations
{
	[InitializeOnLoad]
	public class PreprocessorDirectivesPreparer
	{
		private const char DefineSymbolsSeparatorChar = ';';

		private static readonly string[] DebuggingValidDefines = {
			/* 0 */ "DEVELOPMENT_MODE_ENABLED", // Default
			/* 1 */ "DEVELOPMENT_MODE_DISABLED",
		};

		private static readonly string[] DemonstrationValidDefines = {
			/* 0 */ "DEMO_MODE_DISABLED", // Default
			/* 1 */ "DEMO_MODE_ENABLED",
		};

		private static readonly string[] LoggingValidDefines = {
			/* 0 */ "LOGGING_ENABLED", // Default
			/* 1 */ "LOGGING_DISABLED",
		};

		private static readonly string[] RenderPipelineValidDefines = {
			/* 0 */ "USING_UNITY_BRP", // Fallback
			/* 1 */ "USING_UNITY_URP",
			/* 2 */ "USING_UNITY_HDRP",
		};


		private static HashSet<string> _currentDefineSymbols = new HashSet<string>();




		#region Listeners


			static PreprocessorDirectivesPreparer()
			{
				validatePreprocessorDirectives();
			}


		#endregion




		#region Controls


			[MenuItem("Tools/PossumScream/Preparations/Sort Preprocessor Directives alphabetically")]
			private static void sortPreprocessorDirectivesAlphabetically()
			{
				HLogger.logInfo("Sorting Preprocessor Directives...", typeof(PreprocessorDirectivesPreparer));
				{
					string[] currentDefineSymbols = PlayerSettings.GetScriptingDefineSymbolsForGroup(EditorUserBuildSettings.selectedBuildTargetGroup).Split(DefineSymbolsSeparatorChar);


					Array.Sort(currentDefineSymbols);

					PlayerSettings.SetScriptingDefineSymbolsForGroup(EditorUserBuildSettings.selectedBuildTargetGroup, currentDefineSymbols);
				}
				HLogger.logInfo("Done!", typeof(PreprocessorDirectivesPreparer));
			}


			[MenuItem("Tools/PossumScream/Preparations/Validate Preprocessor Directives")]
			private static void validatePreprocessorDirectives()
			{
				HLogger.logInfo("Updating Preprocessor Directives...", typeof(PreprocessorDirectivesPreparer));
				{
					bool needDefineRevalidation = false;


					_currentDefineSymbols = new HashSet<string>(PlayerSettings.GetScriptingDefineSymbolsForGroup(EditorUserBuildSettings.selectedBuildTargetGroup).Split(DefineSymbolsSeparatorChar));

					{
						setupForAnyValidDefine(DebuggingValidDefines, ref needDefineRevalidation);
						setupForAnyValidDefine(DemonstrationValidDefines, ref needDefineRevalidation);
						setupForAnyValidDefine(LoggingValidDefines, ref needDefineRevalidation);
						setupRenderPipeline(ref needDefineRevalidation);
					}

					if (needDefineRevalidation) {
						PlayerSettings.SetScriptingDefineSymbolsForGroup(EditorUserBuildSettings.selectedBuildTargetGroup, _currentDefineSymbols.ToArray());
					}
				}
				HLogger.logInfo("Done!", typeof(PreprocessorDirectivesPreparer));
			}


			private static bool checkIfAllCurrentPreprocessorDirectivesAreValid()
			{
				_currentDefineSymbols = new HashSet<string>(PlayerSettings.GetScriptingDefineSymbolsForGroup(EditorUserBuildSettings.selectedBuildTargetGroup).Split(DefineSymbolsSeparatorChar));

				{
					if (!_currentDefineSymbols.MatchesAny(DebuggingValidDefines)) return false;
					if (!_currentDefineSymbols.MatchesAny(DemonstrationValidDefines)) return false;
					if (!_currentDefineSymbols.MatchesAny(LoggingValidDefines)) return false;
					if (!_currentDefineSymbols.MatchesAny(RenderPipelineValidDefines)) return false;
				}


				return true;
			}


		#endregion




		#region Actions


			private static void setupForAnyValidDefine(string[] validDefines, ref bool needDefineRevalidation)
			{
				if (_currentDefineSymbols.Matches(validDefines) != 1) {
					removeEveryOtherDefineButFirst(validDefines);
					needDefineRevalidation = true;
				}
			}


			private static void setupRenderPipeline(ref bool needDefineRevalidation)
			{
				string currentRPPreprocessorDirective = PreprocessorDirectivesPreparer.getCurrentRPPreprocessorDirective();


				if (!_currentDefineSymbols.Contains(currentRPPreprocessorDirective) ||
				    (_currentDefineSymbols.Matches(RenderPipelineValidDefines) != 1)) {
					{
						foreach (string define in RenderPipelineValidDefines) {
							_currentDefineSymbols.Remove(define);
						}

						_currentDefineSymbols.Add(currentRPPreprocessorDirective);
					}
					needDefineRevalidation = true;
				}
			}




			private static void removeEveryOtherDefineButFirst(string[] validDefines)
			{
				foreach (string define in validDefines) {
					_currentDefineSymbols.Remove(define);
				}

				_currentDefineSymbols.Add(validDefines[0]);
			}


		#endregion




		#region Utilities


			public static string getCurrentRPPreprocessorDirective()
			{
				if (GraphicsSettings.currentRenderPipeline is null) return RenderPipelineValidDefines[0];


				switch (GraphicsSettings.currentRenderPipeline.GetType().Name) {

					default: {
						return RenderPipelineValidDefines[0];
					}

					case "UniversalRenderPipelineAsset": {
						return RenderPipelineValidDefines[1];
					}

					case "HDRenderPipelineAsset": {
						return RenderPipelineValidDefines[2];
					}

				}
			}


		#endregion
	}
}


#endif




/*                                                                                            */
/*            ____                                 _____                                      */
/*           / __ \____  ____________  ______ ___ / ___/_____________  ____ _____ ___         */
/*          / /_/ / __ \/ ___/ ___/ / / / __ `__ \\__ \/ ___/ ___/ _ \/ __ `/ __ `__ \        */
/*         / ____/ /_/ (__  |__  ) /_/ / / / / / /__/ / /__/ /  /  __/ /_/ / / / / / /        */
/*        /_/    \____/____/____/\__,_/_/ /_/ /_/____/\___/_/   \___/\__,_/_/ /_/ /_/         */
/*                                                                                            */
/*        Licensed under the Apache License, Version 2.0. See LICENSE.md for more info        */
/*        David Tabernero M. @ PossumScream                      Copyright © 2021-2023        */
/*        https://gitlab.com/possumscream                          All rights reserved        */
/*                                                                                            */
/*                                                                                            */