#if UNITY_EDITOR


using PossumScream.SickScripts.Logging;
using UnityEditor;
using UnityEngine;
using UnityObject = UnityEngine.Object;




namespace PossumScream.SickScripts.Optimization
{
	public static class ScaleChecker
	{
		private static int m_allZeroScaleCount = 0;
		private static int m_troublesomeScaleCount = 0;




		#region Controls


			[MenuItem("Tools/PossumScream/Optimization/Check scene for troublesome scales")]
			private static void checkSceneForTroublesomeScales()
			{
				HLogger.logInfo("Checking scene for troublesome scales...", typeof(ScaleChecker));
				{
					Transform[] sceneTransforms = UnityObject.FindObjectsOfType<Transform>(true);


					foreach (Transform transform in sceneTransforms) {
						Vector3 transformLocalScale = transform.localScale;


						if (checkForAllZeroScale(transformLocalScale)) {
							HLogger.logInfo($"Has an <color={HLogger.infoColor}><b>all-zero</b></color> scale", transform);
							m_allZeroScaleCount++;
							continue;
						}

						if (!checkForAllOneScale(transformLocalScale)) {
							HLogger.logWarning($"Has a <color={HLogger.warningColor}><b>troublesome</b></color> scale <b>→ [ x:<color={HLogger.warningColor}>{transformLocalScale.x}</color>, y:<color={HLogger.warningColor}>{transformLocalScale.y}</color>, z:<color={HLogger.warningColor}>{transformLocalScale.z}</color> ]</b>", transform);
							m_troublesomeScaleCount++;
							continue;
						}
					}

					HLogger.logEmphasis($"Analyzed {sceneTransforms.Length} scene scales", typeof(ScaleChecker));
					HLogger.logEmphasis($"Detected {m_allZeroScaleCount} all-zero scales", typeof(ScaleChecker));
					HLogger.logEmphasis($"Detected {m_troublesomeScaleCount} troublesome scales", typeof(ScaleChecker));
				}
				HLogger.logInfo("Done!", typeof(ScaleChecker));
			}


		#endregion




		#region Actions


			private static bool checkForAllZeroScale(Vector3 scale)
			{
				return (Mathf.Approximately(scale.x, 0f) &&
				        Mathf.Approximately(scale.y, 0f) &&
				        Mathf.Approximately(scale.z, 0f));
			}


			private static bool checkForAllOneScale(Vector3 scale)
			{
				return (Mathf.Approximately(scale.x, 1f) &&
				        Mathf.Approximately(scale.y, 1f) &&
				        Mathf.Approximately(scale.z, 1f));
			}


		#endregion
	}
}


#endif




/*                                                                                            */
/*            ____                                 _____                                      */
/*           / __ \____  ____________  ______ ___ / ___/_____________  ____ _____ ___         */
/*          / /_/ / __ \/ ___/ ___/ / / / __ `__ \\__ \/ ___/ ___/ _ \/ __ `/ __ `__ \        */
/*         / ____/ /_/ (__  |__  ) /_/ / / / / / /__/ / /__/ /  /  __/ /_/ / / / / / /        */
/*        /_/    \____/____/____/\__,_/_/ /_/ /_/____/\___/_/   \___/\__,_/_/ /_/ /_/         */
/*                                                                                            */
/*        Licensed under the Apache License, Version 2.0. See LICENSE.md for more info        */
/*        David Tabernero M. @ PossumScream                      Copyright © 2021-2023        */
/*        https://gitlab.com/possumscream                          All rights reserved        */
/*                                                                                            */
/*                                                                                            */