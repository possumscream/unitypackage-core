

# PossumScream Core

## Installation

- Open the **Unity Package Manager**.
- Click the dropdown **+** button and select *Add package from git URL...*.
- Add the following URL [```https://gitlab.com/possumscream/unitypackage-core.git#master```](https://gitlab.com/possumscream/unitypackage-core.git#master) and click **Add**.


## Dependencies

- [```https://github.com/dbrizov/NaughtyAttributes.git#upm```](https://github.com/dbrizov/NaughtyAttributes.git#upm)
- [```https://github.com/possumscream/JetBrainsMono.git#master```](https://github.com/possumscream/JetBrainsMono.git#master)
- [```https://github.com/possumscream/SimpleJSON.git#master```](https://github.com/possumscream/SimpleJSON.git#master)
- [```https://github.com/richardelms/FileBasedPlayerPrefs.git#master```](https://github.com/richardelms/FileBasedPlayerPrefs.git#master)


## License

The base of this package is licensed under the [Apache License, Version 2.0](LICENSE.md).
The dependencies have their own separate licenses.


## Authors

| Name					| Website						| Twitter													|
| ---					| ---							| ---														|
| David Tabernero M.	| https://davidtabernero.net/	| [@davidtabernerom](https://twitter.com/davidtabernerom)	|

